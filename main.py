from datetime import date, datetime, timedelta
from config import sql_connection_string, recipients as to
from dbweathre import WeatherDB
from weather import get_current_temp, get_date_temp
from send import send_email


wdb = WeatherDB(sql_connection_string, reset=False) # False # True # Пересоздать таблицы

N = 0
dt = date.today() - timedelta(days=N)   # Можно сформировать отчёт за предыдущие даты. По умолчанию - сегодня.
# dt = date.fromisoformat('2022-02-22')   # Для удобства, два варианта на выбор: на дату D или N дней назад (см.выше)
days=7
fill_data = False # False # True # Заполнить days дней данными текущего дня (чтобы протестировать работу программы)

cod_error = False

def get_weather(cities):
    for city in cities:
        cod, lat, lon, dt, temp, temp_min, temp_max = get_current_temp(city)
        if cod == 200:
            update_weather_data(lat, lon, dt, city, temp, temp_min, temp_max)

def update_weather_data(lat, lon, dt, city, temp, temp_min, temp_max):
    if fill_data:
        wdb.fill_weather(days, dt, city, temp, temp_min, temp_max)
    else:
        wdb.update_weather(dt, city, temp, temp_min, temp_max)
        fill_weather_dt(lat, lon, dt, city)

def fill_weather_dt(lat, lon, dt, city):
    global cod_error
    for d in range(1, days): # set 5 for free plan
        if cod_error: break
        cod, lat, lon, dt, temp, temp_min, temp_max, temp_avg = get_date_temp(lat, lon, dt)
        if cod == 200: wdb.update_weather(dt, city, temp, temp_min, temp_max)
        if cod == 429: cod_error = True # Error: 429, Your account is temporary blocked due to exceeding of requests limitation of your subscription type.
                                        # Please choose the proper subscription http://openweathermap.org/price
        # FIXME: доработать для других ошибок
        # if cod == 401: cod_error = True
        # if cod == 404: cod_error = True
        # if cod in (500, 502, 503, 504): cod_error = True
        # # https://openweathermap.org/faq#API_errors

def make_html(data):
    rows = [[row['city'], str(round(row['temp'], 2))] for row in data]
    tds = ['<td>'+'</td><td>'.join(td)+'</td>' for td in rows]
    trs = '<tr>'+'</tr>\n<tr>'.join(tds)+'</tr>'
    html = "<!DOCTYPE HTML>\n<html>\n<head></head>\n<body>\n<table>\n" + \
          f"<caption>Average daily temperature for {days} days</caption>" + \
           "<tr><th>City</th><th>Temp</th></tr>\n" + \
            trs + "\n" \
           "</table>\n</body>\n</html>"
    return html

def main():
    subject = ''
    message = ''
    cities = wdb.get_cities()
    get_weather(cities)
    data = wdb.get_weather_avg(dt=dt, days=days)
    if data:
        subject = f"Weather report for {days} days by {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}"
        message = make_html(data)
    send_email(to, subject, message)
    # TODO: добавить лог формирования данных и прикрепить его к письму
    #  Дополнить тему письма датами 'с' 'по'


if __name__ == "__main__":
    main()

