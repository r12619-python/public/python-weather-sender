import pyodbc
from functools import reduce


class MsSqlDB(object):

    def __init__(self, conn_str):
        """ подключение к MSSQL серверу """
        self.conn = pyodbc.connect(conn_str)
        self.cursor = self.conn.cursor()
        self.data = {}

    def __del__(self):
        """ уничтожение объектов класса при удалении экземпляра """
        try: self.cursor.close()
        except: pass
        try: self.conn.close()
        except: pass

    @staticmethod
    def cursor_to_dict(cursor):
        """ Return all rows from a cursor as a dict """
        columns = [col[0] for col in cursor.description]
        res = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
        return res

    @staticmethod
    def cursor_to_list(cursor):
        """ Return all rows from a cursor as a list """
        res = reduce(
                lambda a, b: a.extend(b) or a,
                [row[:] for row in cursor.fetchall()], []
        )
        return res

    def execute_sql(self, sql, *args, **kwargs):
        """ выполняет произвольный запрос к базе, возвращает результат при его наличии """
        self.data = self.cursor.execute(sql, *args, **kwargs)
        return self

    def execute_commit_sql(self, sql, *args, **kwargs):
        """ выполняет произвольный запрос к базе, возвращает результат при его наличии """
        self.execute_sql(sql, *args, **kwargs)
        self.conn.commit()
        return self

    def get_value_from_sql(self, sql, *args, **kwargs):
        """ выполняет SQL запрос и возвращает значение из первого поля первой строки """
        self.execute_sql(sql, *args, **kwargs)
        return self.data.fetchval()

    def to_dict(self):
        """ Возвращает результат выполнения запроса в виде словаря """
        return self.cursor_to_dict(self.data)

    def to_list(self):
        """ Возвращает результат выполнения запроса в виде списка """
        return self.cursor_to_list(self.data)


if __name__ == "__main__":
    pass

